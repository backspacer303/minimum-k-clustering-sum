1) nasumicno razporediti instance u k klastera

2) izracunati aktuelnu sumu

3) za sve parove klastera (Si,Sj) radi:

    za sve tacke T iz Si:

        pomeri T iz Si u Sj
        izracunaj novu sumu 

        ako je nova suma manja od aktuelne:
            da li se aktuelna smanjila vise od prethodno najboljeg pomeranja:
                ako jeste azuriraj najbolje pomeranje

    za sve tacke G iz Si:

        pomeri G iz Sj u Si
        izracunaj novu sumu 

        ako je nova suma manja od aktuelne:
            da li se aktuelna smanjila vise od prethodno najboljeg pomeranja:
                ako jeste azuriraj najbolje pomeranje

4) dobili smo najbolje pomeranje (ono koje smanjuje aktuelnu sumu za najvise)
     -> odardi pomeranje 
     -> azuriraj aktuelnu sumu

5) ponavljaj korake 3 i 4 dokle god ima promena u klasterima

